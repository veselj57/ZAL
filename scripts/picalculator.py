import math


def leibnizPi(members):
    pi = 0.0
    for n in range(1, members+1):
        pi += 4/(1 + 2 * (n-1)) * (-1)**(n-1)
    return pi


def nilakanthaPi(members):
    pi = 3
    for n in range(1, members):
        first_number = 2 + 2*(n-1)
        pi += 4/(first_number * (first_number + 1) * (first_number + 2)) * (-1) ** (n-1)
    return pi


def newtonPi (x_guess):
    last = float(x_guess)
    while True:
        new = last - math.sin(last)/math.cos(last)
        if new == last:
            return new
        else:
            last = new
