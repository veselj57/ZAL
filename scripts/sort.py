import random

def sortNumbers(unsorted, condition):
    return merge_sort(unsorted, asc_translator(condition))


# simple merge sort
def merge_sort(unsorted_list, asc):
    # keep splitting the array with recursion until it is array of single element
    if len(unsorted_list) <= 1:
        return unsorted_list
    else:
        mid_point = len(unsorted_list) // 2

        # syntax take everything to or from mid_point
        left = merge_sort(unsorted_list[:mid_point], asc)
        right = merge_sort(unsorted_list[mid_point:], asc)

        results = []

        # always append the smaller value to the results list,
        # if either one of the arrays is empty append the rest of the second array since it is sorted
        left_index, right_index = 0, 0
        while left_index < len(left) and right_index < len(right):

            if (asc and left[left_index] <= right[right_index]) or (not asc and left[left_index] >= right[right_index]):
                results.append(left[left_index])
                left_index += 1
            else:
                results.append(right[right_index])
                right_index += 1

        # append rest of values to results list
        results += left[left_index:]
        results += right[right_index:]

        return results


def sortData(weights, data, condition):
    if len(weights) != len(data):
        raise ValueError('Invalid input data')
    else:
        return dual_merge_sort(weights, data, asc_translator(condition))['data']


# dual merge sort shift both weights and data
def dual_merge_sort(weights, data_results, asc):
    if len(weights) <= 1:
        return {'weights': weights, 'data': data_results}
    else:
        mid_point = len(weights) // 2

        # syntax take everything to or from mid_point
        left_dict = dual_merge_sort(weights[:mid_point], data_results[:mid_point], asc)
        right_dict = dual_merge_sort(weights[mid_point:], data_results[mid_point:], asc)

        weights_results = []
        data_results = []

        # always append the smaller value to the results list,
        # if either one of the arrays is empty append the rest of the second array since it is sorted
        left_index, right_index = 0, 0
        while left_index < len(left_dict['weights']) and right_index < len(right_dict['weights']):
            if (asc and left_dict['weights'][left_index] <= right_dict['weights'][right_index]) or (
                        not asc and left_dict['weights'][left_index] >= right_dict['weights'][right_index]):

                weights_results.append(left_dict['weights'][left_index])
                data_results.append(left_dict['data'][left_index])
                left_index += 1
            else:
                weights_results.append(right_dict['weights'][right_index])
                data_results.append(right_dict['data'][right_index])
                right_index += 1

        # append rest of values to results list
        weights_results += left_dict['weights'][left_index:]
        weights_results += right_dict['weights'][right_index:]

        data_results += left_dict['data'][left_index:]
        data_results += right_dict['data'][right_index:]

        return {'weights': weights_results, 'data': data_results}


def asc_translator(value):
    if value == 'ASC':
        return True
    elif value == 'DESC':
        return False
    else:
        raise ValueError('Invalid input data')



arr = []

for x in range(0, 99):
    arr.append(random.randint(0, 100))

print(merge_sort(arr, True))



