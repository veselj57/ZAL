class Vertex:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.previous_vertex = None
        self.visited = False;
        self.min_distance = float('inf')
        self.edges = []


class Edge:
    def __init__(self, source, target, weight):
        self.source = source
        self.target = target
        self.weight = weight


class UnvisitedOrderedSet:
    def __init__(self, vertexes, start):
        self.vertex_qeu = vertexes
        self.start_from_id = -1

    def set_start(self, id):
        self.start_from_id = id


    def get_next_vertex(self):
        if self.start_from_id != 1:
            for vertex in self.vertex_qeu:
                if vertex.id == self.start_from_id:
                    return vertex
        else:
            # return next with shortest path
            return None

    def insert(self):
        pass



class Dijkstra:
    def __init__(self):
        self.vertex_qeu = None

    def computePath(self, sourceId):
        self.vertex_qeu.set_start(sourceId)

        vertex = self.vertex_qeu.get_next_vertex()
        while vertex:
            for edge in vertex.edges:


    def getShortestPathTo(self, targetId):
        pass

    def createGraph(self, vertexes, edges):


        # Map edges to vertexes
        for edge in edges:
            for vertex in vertexes:
                if vertex.id == edge.source:
                    vertex.edges.append(edge)
                    break

    def resetDijkstra(self):
        pass

    def getVertexes(self):
        pass

    def inspect_next(self):
        pass


dijkstra = Dijkstra()
vertexes = [
    Vertex(1, 'A'),
    Vertex(2, 'B'),
    Vertex(3, 'C'),
    Vertex(4, 'D'),
    Vertex(5, 'E'),
    Vertex(6, 'F'),
    Vertex(7, 'G'),
    Vertex(8, 'H'),
]

edges = [
    Edge(5, 2, 50),
    Edge(5, 7, 30),
    Edge(7, 1, 20),
    Edge(1, 7, 90),
    Edge(4, 7, 20),
    Edge(1, 4, 80),
    Edge(1, 2, 20),
    Edge(2, 6, 10),
    Edge(6, 4, 40),
    Edge(3, 4, 10),
    Edge(3, 8, 20),
    Edge(3, 6, 50),
    Edge(6, 3, 10),
]

dijkstra.createGraph(vertexes, edges)

for vertex in dijkstra.vertex_qeu:
    print("Vertex " + str(vertex.id) + " has way to :" + str([str(edge.target) for edge in vertex.edges]))








