def polyEval(poly, x):
    table = []
    for n, p in enumerate(reversed(poly)):
        table.append([p, 0, p])
        if n >= 1:
            table[n][1] = table[n - 1][2] * x
        table[n][2] = table[n][0] + table[n][1]

    return table[len(table)-1][2]


def polySum(poly1, poly2):
    # poly2 must be always bigger than poly1
    if len(poly2) <= len(poly1):
        temp = poly1
        poly1 = poly2
        poly2 = temp
        del temp

    # add the smaller array to the bigger. The if determine where the small array ends
    # that means there is nothing to add to the bigger one
    for n, p in enumerate(poly1):
        poly2[n] += p

    zero_stripper(poly2)

    return poly2


def polyMultiply(poly1, poly2):
    # array full of zeros. length  represents the highest power of X
    results = [0] * (len(poly1) + len(poly2) - 1)

    # Increase padding after each move in first for loop.
    # That represents shifting powers of X in polynom.
    zero_padding = 0

    for p1 in poly1:
        for n, p2 in enumerate(poly2):
            results[zero_padding + n] += p2*p1
        zero_padding += 1

    zero_stripper(results)
    return results


# strips any ending zeros from given array
def zero_stripper(polynom):
    for n in range(len(polynom) - 1, 0, -1):
        if polynom[n] == 0:
            del polynom[n]
        else:
            break

