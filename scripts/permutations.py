permutations_array = []


def permutations(array):
    """
    starts permutation recursion and delivers final result
    :param array:
    :return:
    """
    global permutations_array
    permutations_array = []  # Reset array after before using it
    process(array, 0)
    return permutations_array


def process(array, element):
    """
    permutation processor
    :param array:
    :param element:
    :return:
    """
    if element == len(array):
        permutations_array.append(array)
    else:
        for n in range(element, len(array)):
            process(swap(array, element, n), element + 1)


def copy(array):
    """
    makes a copy of an array
    :param array:
    :return: copy of array
    """
    new_copy = []
    for x in array:
        new_copy.append(x)
    return new_copy


def swap(array, index_1, index_2):
    """
    creates a copy and swaps two indexes
    :param array:
    :param index_1:
    :param index_2:
    :return: new array with swapped indexes
    """
    new_copy = copy(array)
    temp = new_copy[index_1]
    new_copy[index_1] = new_copy[index_2]
    new_copy[index_2] = temp
    return new_copy
