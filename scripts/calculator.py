import math


def addition(x, y):
    try:
        return float(x) + float(y)
    except Exception:
        exception_thrower()


def subtraction(x, y):
    try:
        return float(x) - float(y)
    except Exception:
        exception_thrower()


def multiplication(x, y):
    try:
        return float(x) * float(y)
    except Exception:
        exception_thrower()


def division(x, y):
    try:
        return float(x) / float(y)
    except Exception:
        exception_thrower()


def modulo(x, y):
    try:
        x = float(x)
        y = float(y)

        if x >= y > 0:
            return float(x) % float(y)
        else:
            raise Exception
    except Exception:
        exception_thrower()


def secondPower(x):
    return power(x, 2)


def power(x, y):
    try:
        if y < 0:
            raise Exception
        else:
            return float(x) ** float(y)
    except Exception:
        exception_thrower()


def secondRadix(x):
    if x <= 0:
        exception_thrower()

    try:
        return math.sqrt(x)
    except Exception:
        exception_thrower()


def magic(x, y, z, k):
    try:
        l = float(x) + float(k)
        m = float(y) + float(z)

        return (l / m) + 1
    except Exception:
        exception_thrower()


def control(a, x, y, z, k):
    if a == 'ADDITION':
        return addition(x, y)
    elif a == 'SUBTRACTION':
        return subtraction(x, y)
    elif a == 'MULTIPLICATION':
        return multiplication(x, y)
    elif a == 'DIVISION':
        return division(x, y)
    elif a == 'MOD':
        return modulo(x, y)
    elif a == 'POWER':
        return power(x, y)
    elif a == 'SECONDRADIX':
        return secondRadix(x)
    elif a == 'MAGIC':
        return magic(x, y, z, k)
    else:
        exception_thrower()


def exception_thrower():
    raise ValueError('This operation is not supported for given input parameters')

