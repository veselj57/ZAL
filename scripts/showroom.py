class Node:
    """
        Node is used to link items in LinkedList
    """
    def __init__(self, next_node, prev_node, data):
        self.nextNode = next_node
        self.prevNode = prev_node
        self.data = data

    def get_next(self):
        return self.nextNode


class LinkedList:
    """
        LinkedList Implementation
        D
    """
    def __init__(self, items):
        self.size = 0
        self.head = None
        for item in items:
            self.add_item(item)

    def get_root(self):
        """
            Return root element of LinkedList
        """
        return self.head

    # return all items in LinkedList
    def get_items(self):
        """
            Return array of all items in linked list
        """
        items = []
        next_node = self.head

        while next_node:
            items.append(next_node.data)
            next_node = next_node.get_next()

        return items

    # return specific item in LinkedList
    def get_item(self, identification):
        """
            Return item in LinkedList by identification
        """
        return self.get_node(identification).data

    # return all Nodes in LinkedList
    def get_node(self, identification):
        """
            Return Node by identification
        """
        this_node = self.head
        while this_node:
            if this_node.data.identification == identification:
                return this_node
            else:
                this_node = this_node.get_next()
        # raise ValueError("this node doesnt exists")

    # add node with the date to sorted place in LinkedList
    def add_item(self, data):
        if self.size == 0:
            self.head = Node(None, None, data)
            self.size += 1
        else:
            if data.price < self.head.data.price:
                self.head = Node(self.head, None, data)
            else:
                this_node = self.head
                while this_node:
                    next_node = this_node.get_next()

                    if this_node.get_next() is None:
                        this_node.nextNode = Node(None, this_node, data)
                        self.size += 1
                        break

                    if this_node.data.price <= data.price < next_node.data.price:
                        this_node.nextNode = Node(next_node, this_node, data)
                        self.size += 1
                        break

                    this_node = next_node

    def empty(self):
        self.head = None
        self.size = 0


class Car:
    """
        Container which holds information about car
    """
    def __init__(self, identification, name, brand, price, active):
        self.identification = identification
        self.name = name
        self.brand = brand
        self.price = price
        self.active = active
        pass


#scope variable
linked_list = None

def init(cars):
    """
        Initialize linked list with given values
    """
    global linked_list
    linked_list = LinkedList(cars)


def add(car):
    """
        Add new Car to the linked list
    """
    global linked_list
    linked_list.add_item(car)


def updateName(identification, name):
    """
        Update name of the car which is specified by identification
    """
    global linked_list
    node = linked_list.get_node(identification)
    if node != None:
        node.data.name = name


def updateBrand(identification, brand):
    """
        Update car brand which is specified by identification
    """
    global linked_list
    node = linked_list.get_node(identification)
    if node != None:
        node.data.brand = brand


def activateCar(identification):
    """
        Activate car in showroom
    """
    global linked_list
    node = linked_list.get_node(identification)
    if node != None:
        linked_list.get_item(identification).active = True


def deactivateCar(identification):
    """
        Deactivate car in show room
    """
    global linked_list
    node = linked_list.get_node(identification)
    if node != None:
        linked_list.get_item(identification).active = False


def getDatabaseHead():
    """
        Return root element in LinkedList
    """
    global linked_list
    return linked_list.get_root()


def getDatabase():
    """
        return LinkedList Class
    """
    global linked_list
    return linked_list


def calculateCarPrice():
    """
        Calculate price of all active Cars in LinkedList
    """
    global linked_list

    price = 0

    for n in linked_list.get_items():
        if n.active:
            price += n.price
    return price


def clean():
    """
        Empty the LinkedList
    """
    global linked_list
    linked_list.empty()
    pass

"""
def get_cars():
    return [
        Car(1, 'a', 'BMW', 1000, False),
        Car(2, 'b', 'Skoda', 200, True),
        Car(3, 'c', 'Opel', 300, True),
        Car(4, 'd', 'KIA', 400, True)
    ]

init(get_cars())

clean()

print([n.name + "-" + str(n.identification) for n in linked_list.get_items()])

add(Car(1, 'a', 'Octavia2', 1000, True))
add(Car(23, 'a', 'Octavia 2015', 1000, True))
add(Car(24, 'a', 'Octavia 2015', 1000, True))

print([str(n.identification) + "-" + n.name for n in getDatabase().get_items()])

updateName(24, 'xxx')
updateName(1, 'xxx')
print([str(n.identification) + "-" + n.name for n in getDatabase().get_items()])

OUTPUT
['a-1', 'a-23']
['a-1', 'xxx-23'
"""




