import copy


class Node:
    def __init__(self, value, is_black=False, parent=None, left=None, right=None):
        self.value = value
        self.parent = parent
        self.left = left
        self.right = right

        self.is_black = is_black

    def is_left_child(self):
        return self.parent.value > self.value

    def get_uncle(self):
        if self.parent.is_left_child():
            return self.parent.parent.right
        else:
            return self.parent.parent.left

    def is_uncle_black(self):
        uncle = self.get_uncle()
        return uncle is None or uncle.is_black

    def is_black(self):
        return self.is_black


class BinarySearchTree:
    def __init__(self):
        self.root = None
        self.size = 0
        self.last_depth = 0

    def insert(self, value):
        if self.root is None:
            self.root = Node(value, True)
            self.size += 1
        else:
            iter_node = self.root
            new_node = None

            # inserts Node in in place accordion to BST
            while iter_node:
                if value < iter_node.value:
                    if iter_node.left is None:
                        new_node = Node(value)
                        new_node.parent = iter_node
                        iter_node.left = new_node
                        self.size += 1
                        break
                    iter_node = iter_node.left
                else:
                    if iter_node.right is None:
                        new_node = Node(value)
                        new_node.parent = iter_node
                        iter_node.right = new_node
                        self.size += 1
                        break
                    iter_node = iter_node.right

            # I am working here on red black tree
            # self.insert_fix(new_node)

    def left_rotate(self, new_node):
        if new_node.parent.parent == self.root:
            new_node.parent.parent.right = new_node.parent.left
            new_node.parent.left = new_node.parent.parent
            new_node.parent.left.parent = new_node.parent
            self.root = new_node.parent
            self.root.parent = None

            self.root.is_black = True
            self.root.left.is_black = False
        else:
            pass  # !!!!!!!!!!!

    def right_rotate(self, x):
        if x.parent.parent == self.root:
            x.parent.parent.left = x.parent.right
            x.parent.right = x.parent.parent
            x.parent.right.parent = x.parent
            self.root = x.parent
            self.root.parent = None

            self.root.is_black = True
            self.root.right.is_black = False
        else:
            pass  # !!!!!!!!!!!

    def insert_fix(self, fix_node):
        """
        Work in progress
        """
        while not x.parent.is_black:

            if fix_node.parent.is_left_child():  # left side of the tree
                uncle = x.get_uncle()

                if x.is_uncle_black():
                    if not x.is_left_child():
                        grand_parent = x.parent.parent
                        parent = x.parent

                        parent.right = x.left
                        x.left = parent
                        x.parent = grand_parent
                        grand_parent.left = x
                        x = parent

                    self.right_rotate(x)
                else:
                    # case 1 two consecutive reds with red uncle
                    uncle.is_black = True
                    x.parent.is_black = True
                    x.parent.parent.is_black = False

            else:  # right side of the tree
                uncle = x.get_uncle()

                if x.is_uncle_black():
                    if x.is_left_child():
                        grand_parent = x.parent.parent
                        parent = x.parent

                        parent.left = x.right
                        x.right = parent
                        x.parent = grand_parent
                        grand_parent.right = x
                        x = parent

                    self.left_rotate(x)
                else:
                    # case 1 two consecutive reds with red uncle
                    uncle.is_black = True
                    x.parent.is_black = True
                    x.parent.parent.is_black = False

    def fromArray(self, numbers):
        """
        insert nodes from array to BST
        :param List numbers:
        """
        for x in numbers:
            self.insert(x)

    def search(self, value):
        self.last_depth = 1
        iter_node = self.root

        while iter_node:
            if iter_node.value == value:
                return True
            else:
                self.last_depth += 1
                if iter_node.value < value:
                    iter_node = iter_node.right
                else:
                    iter_node = iter_node.left

        self.last_depth -= 1
        return False


    def min(self):
        self.last_depth = 0
        iter_node = self.root
        while iter_node:
            self.last_depth += 1
            if iter_node.left is None:
                return iter_node.value
            else:
                iter_node = iter_node.left

    def max(self):
        self.last_depth = 0
        iter_node = self.root
        while iter_node:
            self.last_depth += 1
            if iter_node.right is None:
                return iter_node.value
            else:
                iter_node = iter_node.right

    def visitedNodes(self):
        return self.last_depth
