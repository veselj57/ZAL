def writeTextToFile(random_text):
    file_name = "file.txt"
    static_text = "This is my static text which must be added to file. It is very long text and I do not know what " \
                  "they want to do with this terrible text. "
    with open(file_name, 'wt') as outputFile:
        outputFile.write(static_text + str(random_text))

    return file_name
