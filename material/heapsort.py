__author__ = 'Tom'
import random


def randArray(size):
    array = [0] * size
    for i in range(len(array)):
        array[i] = random.randint(0, 50)
    return array

array = randArray(20)
print(array)


def heapify(array, end, ind):
    i, j = -1, ind
    while i != j:
        i = j
        if 2 * i + 1 < end and array[2 * i + 1] > array[j]:
            j = 2 * i + 1
        if 2 * i + 2 < end and array[2 * i + 2] > array[j]:
            j = 2 * i + 2
        array[i], array[j] = array[j], array[i]


def heapSort(array):
    array = list(array)

    # heapify
    for i in reversed(range(len(array) // 2)):
        heapify(array, len(array), i)

    for i in reversed(range(len(array))):
        array[0], array[i] = array[i], array[0]
        heapify(array, i, 0)

    return array


print(heapSort(array))